# Need to source this file.

# Pass in audit string, returns formatted block as `aa_log_extract_tokens.pl`
# would have provided. Need to return:
#
# profile:/usr/bin/busctl
# sdmode:REJECTING
# denied_mask:r
# operation:open
# name:/proc/879/stat
# request_mask:r
#
# Note: apparmor uses the term "DENIED" rather than "REJECTING", but our
# expected values want "REJECTING", so tweak for now.
apparmor_parse_journal() {
	awk -v event=$2 '
		{
			for (i = 1; i <= NF; i++) {
				n = index($i, "=");
				if(n) {
					vars[substr($i, 1, n - 1)] = substr($i, n + 1)
					# Strip quotes
					gsub("\"","",vars[substr($i, 1, n - 1)])
				}
			}
		}
		{
			if (vars["apparmor"] == event) {
				print "===="
				print "profile:"vars["profile"]
				# Match old nomenclature
				if (vars["apparmor"] == "DENIED") {
					vars["apparmor"] = "REJECTING"
				}
				print "sdmode:"vars["apparmor"]
				print "denied_mask:"vars["denied_mask"]
				print "operation:"vars["operation"]
				print "name:"vars["name"]
				print "request_mask:"vars["requested_mask"]
			}
		}
	' $1
}
